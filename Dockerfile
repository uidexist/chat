FROM node:13.12.0-alpine3.11
MAINTAINER Echo

RUN mkdir chat
COPY . /chat
WORKDIR /chat
RUN npm i --registry=https://registry.npm.taobao.org

EXPOSE 8003

CMD npm run start
